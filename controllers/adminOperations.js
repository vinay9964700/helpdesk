//const async=require('async')
const setSLA=require('../utils/setSla')
let dbcon=require('../config/db/mysql-config');
let QUERY=require('../schema/admin_schema');
const SEND_MAIL=require('../utils/mailSender')


this.adduser =async (obj,callback)=> {
    
    try {
        await dbcon.query(QUERY.SAVE_USER,obj,function(err,data,fields){
            if(err){
                return callback(true,err)
            }else if(data){
                return callback(false,data)
            }else{
                return callback(true,err)
            }
        })
        }
    catch (error) {
        console.log(error)
        callback(true,error)
    }
}

this.allrequest=async (obj,callback)=> {
    try {
        await dbcon.query(QUERY.ALL_REQUEST,function(err,data){
            if(err){
                callback(true,err);
            }else{
                callback(false,data)
            }
        });
    } catch (error) {
        callback(true,error)
    }
}

this.request_by_id=async (id,cb)=>{
    try {
        await dbcon.query(QUERY.REQ_BY_REQ_ID,id,function(err,data){
            if(err){
                cb(true,err)
            }else{
                cb(false,data)
            }
        })
    } catch (error) {
        cb(true,error)
    }
}



this.request_acceptance = async(obj,callback)=>{

    try {
        let _req_id=obj.req_id
        ,_email=obj.email
        ,_req_type=obj.req_type;

        let sladata={
            req_id:_req_id,
            req_type:_req_type
        }

        //set the sla
        await setSLA(sladata)

        //
        let status="accepted"
        await dbcon.query(QUERY.UPDATE_STATUS_ACCEPT,[status,_req_id])

        //send the mail
        data={
            email:_email,
            mail_type:"1"
        }
        await SEND_MAIL(data)
        callback(false,"mail sent")
        

    } catch (error) {
        console.log(error)
        callback(true,error)
    }

    
}


module.exports=this;
