const adminOperations=require('../controllers/adminOperations');
const httputil=require('../utils/HttpUtil');
const sesCheck=require('../middlewares/SessionCkeck');



function _Routes(prefixPath, app) {


    
    app.post(prefixPath+'/adduser', function (req,res) {
        
            let obj=req.body.email;
            adminOperations.adduser(obj,(err, data) => {
                console.log('from routes'+data)
                if (!err) {
                    res.json(httputil.getCreated(data));
                } else{
                    res.json(httputil.getError(data));
                } 
           });
    });

        
    app.get(prefixPath+'/dashboard', function (req,res) {
        try {
            adminOperations.allrequest(req,(err,data)=>{
                if(err){
                    res.json(httputil.getError(data))
                }else{
                    res.json(httputil.getSuccess(data))
                }
            })
        } catch (error) {
            res.json(httputil.getError(error))
        }
});

     
    app.get(prefixPath+'/dashboard/_id', function (req,res) {
        try {
            let id=req.query._id
            adminOperations.request_by_id(id,(err,data)=>{
                if(err){
                    res.json(httputil.getError(data))
                }else{
                    res.json(httputil.getSuccess(data))
                }
            })
        } catch (error) {
            res.json(httputil.getError(error))
        }
});



    app.post(prefixPath+'/request_operations/accept', function (req,res) {
        try {
            const obj=req.body;
            adminOperations.request_acceptance(obj,(err,data)=>{
                //console.log("from routed"+data)
                console.log(err)
                if(!err){
                    res.json(httputil.getSuccess(data))
                }else{
                    console.log("from then error")
                    res.json(httputil.getError(data))
                }
            })
        } catch (error) {
            console.log("from catch error")
            res.json(httputil.getError(error))
        }
});

 
    app.get(prefixPath+'/admin/:req_id/accept', function (req,res) {
        try {
            
        } catch (error) {
            
        }
});

  
    app.get(prefixPath+'/admin/:req_id/assign_to', function (req,res) {
        try {
            
        } catch (error) {
            
        }
});

  
    app.get(prefixPath+'/admin/:req_id/closing_request', function (req,res) {
        try {
            
        } catch (error) {
            
        }
});


}



module.exports=_Routes;
