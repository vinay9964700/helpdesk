module.exports={
    SAVE_USER:"insert into tbl_user set user_name = ?",
    ALL_REQUEST:"select * from tbl_request",
    REQ_BY_REQ_ID:"select * from tbl_request where req_id = ?",

    SLA_UPDATE_TIME:"update tbl_request set accepted_date = ? , accepted_time = ? , sla = ? where req_id = ?",
    UPDATE_STATUS_ACCEPT:"update tbl_request set status = ? where req_id = ?"
}