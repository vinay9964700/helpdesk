"use strict";


module.exports = {



    getSuccess : (data) => {
        return {
            status: 200,
            result: data,
            message: 'Ok'
        }
    },


    getCreated : (data) => {
        return {
            status: 201,
            result: data,
            message: 'Created'
        }
    },


    getError : (data) => {
        return {
            status: 500,
            result: data,
            message: 'Error'
        }
    },


    getInvalidRequest : (msg) => {
        return {
            status: 400,
            result: "Bad Request",
            message: msg
        }
    }
}