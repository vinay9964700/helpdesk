const dbcon = require('../config/db/mysql-config');
const QUERY = require('../schema/admin_schema');


//@data object with reqid and req
Sla = async (data) => {
   
    let type=data.req_type
    let curdate = new Date().toLocaleDateString();
    let cur_time = new Date().toLocaleTimeString();

    if(type==="1d"){
        try {
            
            let sla = new Date(new Date().getTime() + (1 * 24 * 60 * 60 * 1000)).toLocaleDateString()
            const id=data.req_id
           await dbcon.query(QUERY.SLA_UPDATE_TIME, [curdate, cur_time, sla,id])     

        } catch (error) {
            console.log(error)
        }
    }
    else if(type==="4h"){
        console.log("from 4h")
        try {
            
            let sla = new Date(new Date().getTime() + (4 * 60 * 60 * 1000)).toLocaleDateString()
            const id=1;

            await dbcon.query(QUERY.SLA_UPDATE_TIME, [curdate, cur_time, sla,id])
                           

        } catch (error) {
            console.log(error)
        }
    }
    else if(type==="3d"){
        console.log("from 3d")
        try {
            
            let sla = new Date(new Date().getTime() + (3 * 24 * 60 * 60 * 1000)).toLocaleDateString()
            const id=1;

            await dbcon.query(QUERY.SLA_UPDATE_TIME, [curdate, cur_time, sla,id])
                        

        } catch (error) {
            console.log(error)
        }
    }
    else if(type==="2h"){
        console.log("from 2h")
        try {
            
            let sla = new Date(new Date().getTime() + (2 * 60 * 60 * 1000)).toLocaleDateString()
            const id=1;

            await dbcon.query(QUERY.SLA_UPDATE_TIME, [curdate, cur_time, sla,id])
                       

        } catch (error) {
            console.log(error)
        }
    }
    else if(type==="3h"){
        console.log("from 3h")
        try {
            
            let sla = new Date(new Date().getTime() + (3 * 60 * 60 * 1000)).toLocaleDateString()
            const id=1;

            await dbcon.query(QUERY.SLA_UPDATE_TIME, [curdate, cur_time, sla,id])
                          

        } catch (error) {
            console.log(error)
        }
    }
    else{
        console.log("does not match")   
    }
  }


module.exports = Sla